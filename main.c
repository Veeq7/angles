#include "stdio.h"
#include "string.h"
#include "math.h"
#include "stdarg.h"

#define PI 3.14159265

typedef struct {
    int x;
    int y;
} v2;

v2 v2_rotate(v2 vec, double angle) {
    double rad = PI / 180.0 * angle;
    float fx = vec.x * cos(rad) - vec.y * sin(rad);
    float fy = vec.y * cos(rad) + vec.x * sin(rad);
    return (v2){ fx, fy };
}

#define OffsetCap 64

int scan_line(const char* const format, ...) {
    char buffer[128];
    fgets(buffer, 128, stdin);
    strtok(buffer, "\r\n");
    
    va_list va = {0};
    va_start(va, format);
    return vsscanf(buffer, format, va);
}

// Print to stdout and to `file` if specified
void print_out(FILE* file, const char* format, ...) {
    va_list va = {0};
    va_start(va, format);
    vprintf(format, va);
    if (file) vfprintf(file, format, va);
}

int main(int argc, char** argv) {
    int frames = 0;
    while (frames != 17 && frames != 32) {
        printf("Input the total amount of frames (must be 17 or 32): ");
        scan_line("%i", &frames);
    }
    printf("\n");
    
    float y_ratio = 1;
    printf("Input the Y ratio (default is 1, isometric is 0.5): ");
    scan_line("%f", &y_ratio);
    printf("\n");

    printf("Input the rotation origin (x y): ");
    v2 origin = {0};
    scan_line("%i %i", &origin.x, &origin.y);
    origin.y = -origin.y;
    printf("\n");

    printf("Input the offsets (forward sideways).\n");
    int offset_count = 0;
    v2 offsets[OffsetCap] = {0};
    while (offset_count < OffsetCap) {
        printf("Offset #%i: ", offset_count + 1);

        v2 current = { 0 };
        if (scan_line("%i %i", &current.y, &current.x) <= 0) {
            break;
        }
        current.y = -current.y;
        offsets[offset_count++] = current;
    }

    FILE* file = 0;
    fopen_s(&file, "angles.txt", "wb");
    if (offset_count) {
        for (int i = 0; i < frames; ++i) {
            print_out(file, "Frame:\n");
            for (int j = 0; j < offset_count; ++j) {
                v2 rotated_offset = v2_rotate(offsets[j], 360.0 / 32 * i);
                print_out(file, "    (%i, %i)\n", origin.x + rotated_offset.x, (int)roundf(origin.y + y_ratio * rotated_offset.y));
            }
            print_out(file, "\n");
        }
    }
    if (file) fclose(file);

    return 0;
}